# DO NOT FORK YOUR SOLUTION #
***Creating a fork will allow others to see your solution***

Please zip up your solution and send it back via your agent/contact.

# README #

### Date Diff Coding Test ###

The aim of the test is to calculate the difference in days between 2 string dates (yyyyMMdd) without the use of external libraries, SimpleDateFormat, Java.util.date or any "date" libraries. The aim is to handle the dates and the calculation yourself.

Check out and import into your IDE as a maven project.

Time spent should be 30-60 mins. Don't worry about handling ALL edge cases if you run out of time.

Let your contact know when you have completed the task.